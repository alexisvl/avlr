#######################################################################
# CONFIG: Main configuration - what to build and what to make it into
#
# Define in your Makefile:
#
# INCLUDES := a b ...
# PROJECT := projectname
# OUTPUTS := elf disas hex bin cflow
# SOURCES := a b c ...
# AVLR := path/to/avlr
# ATPACK := ... (glob to pull in an atpack, see avlr/atpacks for included ones)
#
# # Optional:
# USE_XC8 := /path/to/xc8/install/dir
#
# MCU := ...
# F_CPU := ...
# FLASHSZ := ... (in B)
# FDATSZ := ... (in B)
# RAMSZ := ... (in B)
# EESZ := ... (in B)
#
# (Note that FDATSZ refers to the .fdat section, used in my custom
#  linker scripts to map static const data into the part of flash that
#  has a window in data space on AVR-DX. If not present, omit.)
#
# If the 'cflow' output is provided, cflow must be installed.
#
# Optionally, define:
# OPT_FLAGS := ...
# WARN_FLAGS := ...
# MACH_FLAGS := ...
# EXTRA_FLAGS := ...
# CSTD := ... (defaults to gnu11)
# CXXSTD := ... (defaults to gnu++17)
#
# The following definitions will be provided:
#
# GIT_HASH: the full git hash we are building
# GIT_SHORT_HASH: abbreviated hash
# GIT_INFO: string describing hash (${GIT_SHORT_HASH} plus an optional -dirty)
# GIT_DIRTY: defined to 1 if dirty, else undefined

#######################################################################
# CONFIG: Build configuration
OPT_FLAGS   ?= -O2
WARN_FLAGS  ?= -Wall -Wextra -Werror
MACH_FLAGS  ?= -mrelax -maccumulate-args
CSTD        ?= gnu11
CXXSTD      ?= gnu++17

ifdef USE_XC8
TOOLCH_FLAGS = -mcpu=${MCU} -mdfp=${SYSROOT}/xc8
else
TOOLCH_FLAGS = -B${SYSROOT} --sysroot=${SYSROOT} -mmcu=${MCU}
endif

HASH := $(shell git rev-parse HEAD)
SH_HASH := $(shell git rev-parse --short HEAD)
ifeq ($(strip $(shell git diff --stat)),)
DIRTY_DEF :=
GIT_INFO := ${SH_HASH}
else
DIRTY_DEF := -DGIT_DIRTY=1
GIT_INFO := ${SH_HASH}-dirty
endif

FLAGS = \
	${TOOLCH_FLAGS} \
	-ggdb \
	${OPT_FLAGS} \
	${WARN_FLAGS} \
	${MACH_FLAGS} \
	${CFLOW_FLAGS} \
	${INCLUDES:%=-I%} \
	${EXTRA_FLAGS} \
	-DF_CPU=${F_CPU}uL \
	-DGIT_HASH='"${HASH}"' \
	-DGIT_SHORT_HASH='"${SH_HASH}"' \
	-DGIT_INFO='"${GIT_INFO}"' \
	${DIRTY_DEF} \

CFLAGS = ${FLAGS} -std=${CSTD}
CXXFLAGS = ${FLAGS} -std=${CXXSTD}
FDATSZ ?= 0
BUILD_DIR := build-${PROJECT}
OUTPUT := ${PROJECT}.elf
OUTPUT_FILES := $(patsubst %,${PROJECT}.%,${OUTPUTS})
ATPACK_FILE := $(wildcard ${ATPACK})

#######################################################################
# CONFIG: Toolchain paths
ifdef USE_XC8
XC8_DIR := $(wildcard ${USE_XC8}/v*)
CC      := ${XC8_DIR}/bin/xc8-cc
CXX     := ${XC8_DIR}/bin/xc8-cc
OBJDUMP := ${XC8_DIR}/bin/avr-objdump
OBJCOPY := ${XC8_DIR}/bin/avr-objcopy
else
TARGET  ?= avr-
CC      := ${TARGET}gcc
CXX     := ${TARGET}g++
OBJDUMP := ${TARGET}objdump
OBJCOPY := ${TARGET}objcopy
endif

MH      := python3 ${AVLR}/makehelp.py
SYSROOT ?= .atpack

#######################################################################
# Derived lists
obj1    := $(patsubst %.cpp,${BUILD_DIR}/%.o,${SOURCES})
obj2    := $(patsubst %.c,${BUILD_DIR}/%.o,${obj1})
obj3    := $(patsubst %.s,${BUILD_DIR}/%.o,${obj2})
OBJECTS := ${obj3}

dep1    := $(patsubst %.cpp,${BUILD_DIR}/%.d,${SOURCES})
dep2    := $(patsubst %.c,${BUILD_DIR}/%.d,${dep1})
dep3    := $(filter-out %.s,${dep2})
DEPS    := ${dep3}

#######################################################################
# Support code

# If make runs ${MH} capturing output, it'll blow up when it tries to get the
# terminal width. This passes it in instead.
MH_FIXCOLS = ${MH} -c$(shell tput cols)
# Function to run ${MH} right now - outside a target - and print the output.
RUNMH = $(info $(shell ${MH_FIXCOLS} $(1)))
# Suffix gcc calls with this to format them nicely for ${MH}
GCC_FORMAT := -fdiagnostics-color=always -fmessage-length=$(shell tput cols)

ifeq (${MAKECMDGOALS},clean)
NODEP := 1
else
NODEP := 0
endif

#######################################################################
# HELLO... Compiling ${OUTPUT}
# ........
# blah blah blah
$(call RUNMH,hbar)
ifeq (${MAKECMDGOALS},clean)
$(call RUNMH,info :magenta:HELLO 🗑️ Cleaning up)
else
$(call RUNMH,info :magenta:HELLO 🛠️ Compiling ${OUTPUT})
$(call RUNMH,info :magenta: " ")
$(call RUNMH,info :magenta:target ${MCU} @ Fcpu = ${F_CPU})
$(call RUNMH,info :magenta:stds ${CSTD} ${CXXSTD})
$(call RUNMH,info :magenta:opt ${OPT_FLAGS})
$(call RUNMH,info :magenta:warn ${WARN_FLAGS})
$(call RUNMH,info :magenta:machine ${MACH_FLAGS})
$(call RUNMH,info :magenta:xflags ${EXTRA_FLAGS})
$(call RUNMH,info :magenta:include ${INCLUDES})
endif
$(call RUNMH,hbar)

#######################################################################
# Targets
.PHONY: all clean

${BUILD_DIR}/%.d: %.cpp
	@mkdir -p $$(dirname $@)
	@${MH} run :cyan:DEP $< -- ${CXX} -MM ${CXXFLAGS} $< -c -o $@ ${GCC_FORMAT} && \
	sed -i -e "1s,^,$$(dirname $@)/," $@

${BUILD_DIR}/%.d: %.c
	@mkdir -p $$(dirname $@)
	@${MH} run :cyan:DEP $< -- ${CC} -MM ${CFLAGS} $< -c -o $@ ${GCC_FORMAT} && \
	sed -i -e "1s,^,${BUILD_DIR}/," $@

all: ${SYSROOT} ${OUTPUT_FILES}
	@${MH} hbar
	@${MH} info :green:DONE 🎉💃✔️ ${OUTPUT_FILES}

ifeq (${NODEP},0)
-include ${DEPS}
endif

${OUTPUT}: ${OBJECTS}
	@${MH} run LINK "$@ ← $^" -- ${CXX} ${CFLAGS} $^ -o $@
	@${MH} info :green:SIZE ${OUTPUT} on ${MCU}
	@${MH} size ${OUTPUT} ${MCU} ${FLASHSZ} ${FDATSZ} ${RAMSZ} ${EESZ}

%.disas: %.elf
	@${MH} info OBJDUMP $@
	@${OBJDUMP} -SC $< > $@

%.hex: %.elf
	@${MH} info OBJCOPY $@
	@${OBJCOPY} -O ihex $< $@

%.bin: %.elf
	@${MH} info OBJCOPY $@
	@${OBJCOPY} -O binary $< $@

%.cflow: ${SOURCES}
	@${MH} info CFLOW $^
	cflow ${SOURCES} > $@

${BUILD_DIR}/%.o: %.cpp
	@mkdir -p $$(dirname $@)
	@${MH} run CXX $< -- ${CXX} ${CXXFLAGS} -c -o $@ $(<:.d=.cpp) ${GCC_FORMAT}

${BUILD_DIR}/%.o: %.c
	@mkdir -p $$(dirname $@)
	@${MH} run CC $< -- ${CC} ${CFLAGS} -c -o $@ $(<:.d=.c) ${GCC_FORMAT}

${BUILD_DIR}/%.o: %.s
	@mkdir -p $$(dirname $@)
	@${MH} run AS $< -- ${CC} ${CFLAGS} -x assembler-with-cpp -c -o $@ $(<:.d=.s) ${GCC_FORMAT}

clean:
	@${MH} info :cyan:RM ${OUTPUT_FILES} ${OBJECTS} ${DEPS} ${BUILD_DIR}
	@rm -f ${OUTPUT_FILES} ${OBJECTS} ${DEPS}
	@rm -rf ${BUILD_DIR}

.atpack:
	mkdir $@
	cd $@ && unzip ../${ATPACK_FILE}
	cd $@/gcc && cp -R dev/${MCU}/* .
	cd $@/include/avr && ln -s . legacyio
