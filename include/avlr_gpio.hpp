// intellectual property is bullshit bgdc

#ifndef AVLR_GPIO_HPP
#define AVLR_GPIO_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avlr {

/// A single GPIO.
struct gpio {
	/// Set the GPIO value. Does not enable output driver.
	virtual void set(bool value) = 0;

	/// Return the real GPIO value on the pin.
	virtual bool get() = 0;

	/// Toggle the GPIO.
	virtual void toggle();

	/// Set the GPIO driven
	virtual void drive(bool driven = true) = 0;

	/// Set the GPIO undriven
	void release() { drive(false); }

	/// Enable or disable pullup
	virtual void pullup(bool pu_en) = 0;
};

/// A group of GPIOs. Native implementations probably require these to be on
/// the same port.
template<typename PortReg>
struct gpio_group {
	/// Set all pins in the group according to the specified value. Bits
	/// outside the group will be ignored.
	virtual void set(PortReg value) = 0;

	/// Get all pins in the group. Other bits in the return value will be
	/// zero.
	virtual PortReg get() = 0;

	/// Toggle pins in the group whose corresponding bits are high.
	virtual void toggle(PortReg pins);

	/// Set some pins in the group, following a mask. Bits set in the mask
	/// but not present in the group will be ignored.
	virtual void set_masked(PortReg mask, PortReg value = 0);

	/// Set some pins in the group driven
	virtual void drive(PortReg pins, bool driven = true) = 0;

	/// Set some pins in the group undriven
	void release(PortReg pins) { drive(pins, false); }

	/// Enable or disable pullup on some pins
	virtual void pullup(PortReg pins, bool pu_en) = 0;
};

}

#endif // !defined(AVLR_GPIO_HPP)
