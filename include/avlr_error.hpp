// intellectual property is bullshit bgdc

#ifndef AVLR_ERROR_HPP
#define AVLR_ERROR_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

namespace avlr {

#ifdef X
# error "Need X undefined to use it with X-macros. Where'd it come from?"
#endif

// X macros - this table is mentioned with different definitions of X() to
// do different things with it.
//
// Priorities encode support. The following may be defined at compile time, and
// will change which error codes have their short and long descriptions stored
// for decoding. More common errors have lower numbers.
//
//   - AVLR_ERROR_PRIO_SHORT (default = 2) - maximum priority whose short error
//       codes are included.
//   - AVLR_ERROR_PRIO_LONG (default = 0) - maximum priority whose long error
//       codes are included.
//
//        priority
//        |  error code
//        |  |               error number
//        |  |               |  description string
//        |  |               |  |
#define AVLR_ERROR_TABLE \
	X(1, OK,             0, "OK") \
	X(1, EPERM,          1, "Not permitted") \
	X(1, ENOENT,         2, "No such entry") \
	X(2, ESRCH,          3, "No such process") \
	X(2, EINTR,          4, "Interrupted") \
	X(1, EIO,            5, "IO error") \
	X(1, ENXIO,          6, "No such device") \
	X(2, E2BIG,          7, "Arg list too long") \
	X(4, ENOEXEC,        8, "Exec format error") \
	X(3, EBADF,          9, "Bad file number") \
	X(4, ECHILD,        10, "No child processes") \
	X(1, EAGAIN,        11, "Try again") \
	X(1, ENOMEM,        12, "Out of memory") \
	X(2, EACCES,        13, "Permission denied") \
	X(3, EFAULT,        14, "Bad address") \
	X(4, ENOTBLK,       15, "Block device required") \
	X(1, EBUSY,         16, "Busy") \
	X(1, EEXIST,        17, "File exists") \
	X(4, EXDEV,         18, "Cross-device link") \
	X(2, ENODEV,        19, "No such device") \
	X(4, ENOTDIR,       20, "Not a directory") \
	X(4, EISDIR,        21, "Is a directory") \
	X(1, EINVAL,        22, "Invalid argument") \
	X(2, ENFILE,        23, "File table overflow") \
	X(4, EMFILE,        24, "Too many open files") \
	X(4, ENOTTY,        25, "Not a typewriter") \
	X(4, ETXTBSY,       26, "Text file busy") \
	X(3, EFBIG,         27, "File too large") \
	X(1, ENOSPC,        28, "No space left") \
	X(3, ESPIPE,        29, "Illegal seek") \
	X(4, EROFS,         30, "Read-only file system") \
	X(4, EMLINK,        31, "Too many links") \
	X(4, EPIPE,         32, "Broken pipe") \
	X(1, EDOM,          33, "Arg out of range") \
	X(1, ERANGE,        34, "Result not representable") \
	X(2, EDEADLK,       35, "Operation would deadlock") \
	X(3, ENAMETOOLONG,  36, "Name too long") \
	X(3, ENOLCK,        37, "No locks available") \
	X(2, ENOSYS,        38, "Not implemented") \
	X(4, ENOTEMPTY,     39, "Directory not empty") \
	X(4, ELOOP,         40, "Too many symbolic links") \
	X(4, EWOULDBLOCK,   41, "Operation would block") \
	X(2, ENOMSG,        42, "No message") \
	X(1, ENODATA,       61, "No data") \
	X(3, ETIME,         62, "Timer expired") \
	X(2, EPROTO,        71, "Protocol error") \
	X(3, EBADMSG,       74, "Not a data message") \
	X(3, EOVERFLOW,     75, "Value too large for type") \
	X(4, EILSEQ,        84, "Illegal byte sequence") \
	X(3, EMSGSIZE,      90, "Message too long") \
	X(3, EOPNOTSUPP,    95, "Operation not supported") \
	X(3, EADDRINUSE,    98, "Address in use") \
	X(3, EADDRNOTAVAIL, 99, "Address not available") \
	X(2, ENOBUFS,      105, "No buffer space") \
	X(1, ETIMEDOUT,    110, "Timed out") \
	X(3, EALREADY,     114, "Already in progress") \
	X(3, EINPROGRESS,  115, "Now in progress") \
	X(3, ECANCELED,    125, "Operation canceled") \


enum class error : uint8_t {
# define X(prio_, short_, num_, long_) short_ = num_,
	AVLR_ERROR_TABLE
# undef X
};

/// Get the short code for an error. If the error code is not known, or if
/// short codes are not included in the build for this priority, returns null.
char const * error_name(error e);

/// Get the long description for an error. If the error code is not known, or
/// if descriptions are not included in the build for this priority, returns
/// the short code instead. If the short code is also not available (see
/// error_name), returns null.
char const * error_describe(error e);

}

#endif // !defined(AVLR_ERROR_HPP)
