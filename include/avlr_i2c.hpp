// intellectual property is bullshit bgdc

#ifndef AVLR_I2C_HPP
#define AVLR_I2C_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

#include "avlr_error.hpp"

namespace avlr {

/// Standardized I2C driver.
struct i2c {
	/// Perform a bidirectional transaction. Either buffer may be empty
	/// if it is not used. If receiving data, a repeated start will be
	/// issued automatically.
	///
	/// @param addr - target address, right-aligned
	/// @param data_tx - data to send
	/// @param data_tx_len - length of the data
	/// @param data_rx - buffer for data received
	/// @param data_rx_len - length to receive
	///
	/// @retval avlr::error::OK    - success
	/// @retval avlr::error::ENXIO - device does not ack its address
	/// @retval avlr::error::EIO   - device does not ack received bytes
	virtual error transact(
		uint8_t addr,
		uint8_t const * data_tx,
		uint8_t data_tx_len,
		uint8_t * data_rx,
		uint8_t data_rx_len
	) = 0;
};

}

#endif // !defined(AVLR_I2C_HPP)
