// intellectual property is bullshit bgdc

#ifndef AVLR_I2C_BITBANG_HPP
#define AVLR_I2C_BITBANG_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include "avlr_i2c.hpp"
#include "avlr_gpio.hpp"

namespace avlr {

/// I2C driver using GPIOs to bitbang.
struct i2c_bitbang: public i2c {

	/// Construct.
	///
	/// @param sda - a GPIO for SDA
	/// @param scl - a GPIO for SCL
	i2c_bitbang(gpio & sda, gpio & scl);

	void init();

	virtual error transact(
		uint8_t addr,
		uint8_t const * data_tx,
		uint8_t data_tx_len,
		uint8_t * data_rx,
		uint8_t data_rx_len
	) override;

private:
	gpio & m_sda;
	gpio & m_scl;
	bool m_active;

	void _scl_set(bool v);
	void _sda_set(bool v);
	void _start();
	void _stop();
	bool _get_put_bit(bool bit);
	uint8_t _get_put_byte(uint8_t byte);
};

}

#endif // !defined(AVLR_I2C_BITBANG_HPP)
