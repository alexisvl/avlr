// steal this file

#ifndef AVLR_I2C_H
#define AVLR_I2C_H

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <avr/io.h>

#define AVLR_I2C_ERR_NACK_ADDR 10
#define AVLR_I2C_ERR_NACK_TX   11

struct avlr_i2c_settings;

/// Standardized I2C driver. Holds its own methods; call them with a pointer
/// to the driver instance. To make a driver implementation, "subclass" this by
/// embedding it as the first member in a new struct.
///
/// Available implementations:
///   - avlr_i2c_avrdx.h (src/avlr_i2c_avrdx.c)
struct avlr_i2c {
	/// Initialize the driver. Not all options may be supported (for
	/// example, the bitbang driver may only support very rough speed
	/// settings)
	///
	/// @return 0 on success, negative error values on error
	int (*init)(
		struct avlr_i2c * i2c,
		struct avlr_i2c_settings const * settings
	);

	/// Perform a bidirectional transaction. Either buffer may be empty
	/// if it is not used. If receiving data, a repeated start will be
	/// issued automatically.
	///
	/// @param addr - target address, right-aligned.
	/// @param data_tx - data to send
	/// @param data_tx_len - length of the data
	/// @param data_rx - buffer for data received
	/// @param data_rx_len - length to receive
	///
	/// @return number of bytes received on success, negative error values
	/// on error. A NACK terminating receive is not an error
	int (*transact)(
		struct avlr_i2c * i2c,
		uint8_t addr,
		uint8_t const * data_tx,
		uint8_t data_tx_len,
		uint8_t * data_rx,
		uint8_t data_rx_len
	);
};


/// I2C driver settings. Subclass this in the same way for your implementation.
struct avlr_i2c_settings {
	uint16_t speed_khz;
};


#endif // !defined(AVLR_I2C_H)
