// intellectual property is bullshit bgdc

#ifndef AVLR_GPIO_PORT_HPP
#define AVLR_GPIO_PORT_HPP

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <assert.h>
#include <avr/io.h>
#include <util/atomic.h>
#include "avlr_gpio.hpp"

namespace avlr {

/// base class for anything with a PORT_t and VPORT_t
template<char PortName>
struct has_port_t {
	static_assert(false
#ifdef PORTA
		|| PortName == 'A'
#endif
#ifdef PORTB
		|| PortName == 'B'
#endif
#ifdef PORTC
		|| PortName == 'C'
#endif
#ifdef PORTD
		|| PortName == 'D'
#endif
#ifdef PORTE
		|| PortName == 'E'
#endif
#ifdef PORTF
		|| PortName == 'F'
#endif
	, "PortName must be a valid port letter");

	/// Return the underlying PORT_t
	static constexpr PORT_t * port()
	{
#ifdef PORTA
		if (PortName == 'A') return &PORTA;
#endif
#ifdef PORTB
		if (PortName == 'B') return &PORTB;
#endif
#ifdef PORTC
		if (PortName == 'C') return &PORTC;
#endif
#ifdef PORTD
		if (PortName == 'D') return &PORTD;
#endif
#ifdef PORTE
		if (PortName == 'E') return &PORTE;
#endif
#ifdef PORTF
		if (PortName == 'F') return &PORTF;
#endif
	}

	/// Return the underlying VPORT_t
	static constexpr VPORT_t * vport()
	{
#ifdef VPORTA
		if (PortName == 'A') return &VPORTA;
#endif
#ifdef VPORTB
		if (PortName == 'B') return &VPORTB;
#endif
#ifdef VPORTC
		if (PortName == 'C') return &VPORTC;
#endif
#ifdef VPORTD
		if (PortName == 'D') return &VPORTD;
#endif
#ifdef VPORTE
		if (PortName == 'E') return &VPORTE;
#endif
#ifdef VPORTF
		if (PortName == 'F') return &VPORTF;
#endif
	}
};

/// avlr::gpio implementation for AVRs with a PORT_t (like xmega, avr-dx)
template<char PortName, unsigned PortPin>
struct gpio_port: public gpio, has_port_t<PortName> {
	static_assert(PortPin < 8, "PortPin must be 0 to 7");

	static constexpr uint8_t bitmask = 1u << PortPin;

	gpio_port() {}

	using has_port_t<PortName>::vport;
	using has_port_t<PortName>::port;

	virtual void set(bool value) override
	{
		if (value)
			vport()->OUT |= bitmask;
		else
			vport()->OUT &= ~bitmask;
	}

	virtual bool get() override
	{
		return vport()->IN & bitmask;
	}

	virtual void toggle() override
	{
		port()->OUTTGL = bitmask;
	}

	virtual void drive(bool driven = true) override
	{
		if (driven)
			vport()->DIR |= bitmask;
		else
			vport()->DIR &= ~bitmask;
	}

	virtual void pullup(bool pu_en) override
	{
		(&port()->PIN0CTRL)[PortPin] = pu_en ? PORT_PULLUPEN_bm : 0;
	}

};

/// avlr::gpio_group implementation for AVRs with a PORT_t (like xmega, avr-dx)
template<char PortName, uint8_t BitMask>
struct gpio_group_port: public gpio_group<uint8_t>, has_port_t<PortName> {
	gpio_group_port() {}

	using has_port_t<PortName>::vport;
	using has_port_t<PortName>::port;

	virtual void set(uint8_t value) override
	{
		set_masked(0xFF, value);
	}

	virtual uint8_t get() override
	{
		return vport()->IN & BitMask;
	}

	virtual void toggle(uint8_t pins) override
	{
		port()->OUTTGL = pins & BitMask;
	}

	virtual void set_masked(uint8_t mask, uint8_t value)
	{
		uint8_t out;

		if (BitMask == 0xFF)
		{
			out = vport()->OUT;
			out &= ~mask;
			out |= (value & mask);
			vport()->OUT = out;
		}
		else
		{
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
			{
				uint8_t out = vport()->OUT;
				out &= ~(BitMask & mask);
				out |= (value & BitMask & mask);
				vport()->OUT = out;
			}
		}
	}

	virtual void drive(uint8_t pins, bool driven = true) override
	{
		if (driven)
			vport()->DIR |= pins & BitMask;
		else
			vport()->DIR &= ~(pins & BitMask);
	}

	virtual void pullup(uint8_t pins, bool pu_en) override
	{
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			port()->PINCONFIG = pu_en ? PORT_PULLUPEN_bm : 0;
			port()->PINCTRLUPD = pins & BitMask;
		}
	}
};

}

#endif // !defined(AVLR_GPIO_PORT_HPP)
