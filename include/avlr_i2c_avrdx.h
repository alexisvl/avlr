// copyright hereby assigned to my ass

#ifndef AVLR_I2C_AVRDX_H
#define AVLR_I2C_AVRDX_H

#include "avlr_i2c.h"
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

struct avlr_i2c_avrdx {
	struct avlr_i2c pi2c;
	TWI_t * twi;
};

struct avlr_i2c_settings_avrdx {
	struct avlr_i2c_settings psettings;

	// PINDEF() for SDA. Will set up PORTMUX. init will return -EINVAL if
	// you give an invalid configuration.
	uint8_t pin_sda;
	uint8_t pin_scl;

	// Baud rate register. See datasheet for computation.
	uint8_t mbaud;
};

#ifdef TWI0
extern struct avlr_i2c * const AVLR_I2C_TWI0;
#endif

#ifdef TWI1
extern struct avlr_i2c * const AVLR_I2C_TWI1;
#endif

#endif // !defined(AVLR_I2C_AVRDX_H)
