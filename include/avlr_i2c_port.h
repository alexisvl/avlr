// bgdc yo

#ifndef AVLR_I2C_VPORT_H
#define AVLR_I2C_VPORT_H

#include "avlr_i2c.h"
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>

extern "C" {

/// Bitbang I2C implementation for AVRs that have PORTn->OUTSET etc
///
/// Note that frequency is uncontrolled, we just go as fast as we can.
/// Usually that's not very fast.
struct avlr_i2c_port {
	struct avlr_i2c pi2c;
	PORT_t * port;
	uint8_t bit_sda; // bit, not bit number (0x40, not 6)
	uint8_t bit_scl; // bit, not bit number (0x80, not 7)
	bool active;
};

/// There are no settings
struct avlr_i2c_settings_port {
	struct avlr_i2c_settings settings;
};

/// Instantiate an avlr_i2c_port
struct avlr_i2c * avlr_i2c_port_new(
	struct avlr_i2c_port * i2c,
	PORT_t * port,
	uint8_t bit_sda,
	uint8_t bit_scl
);

}

#endif // !defined(AVLR_I2C_VPORT_H)
