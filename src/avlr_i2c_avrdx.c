// whatever idc

#include "avlr_i2c_avrdx.h"
#include "avlr.h"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <avr/io.h>
#include <errno.h>

static int _init(
	struct avlr_i2c * i2c,
	struct avlr_i2c_settings const * settings
);

static int _transact(
	struct avlr_i2c * i2c,
	uint8_t addr,
	uint8_t const * data_tx,
	uint8_t data_tx_len,
	uint8_t * data_rx,
	uint8_t data_rx_len
);

#ifdef TWI0
static struct avlr_i2c_avrdx _AVLR_I2C_TWI0 = {
	.pi2c = {
		.init = _init,
		.transact = _transact,
	},
	.twi = &TWI0,
};

struct avlr_i2c * const AVLR_I2C_TWI0 = &_AVLR_I2C_TWI0.pi2c;
#endif

#ifdef TWI1
static struct avlr_i2c_avrdx _AVLR_I2C_TWI1 = {
	.pi2c = {
		.init = _init,
		.transact = _transact,
	},
	.twi = &TWI1,
};

struct avlr_i2c * const AVLR_I2C_TWI1 = &_AVLR_I2C_TWI1.pi2c;
#endif

static int _init(
	struct avlr_i2c * i2c,
	struct avlr_i2c_settings const * settings
)
{
	struct avlr_i2c_avrdx * dxi2c = (struct avlr_i2c_avrdx *) i2c;
	struct avlr_i2c_settings_avrdx const * dxsett =
		(struct avlr_i2c_settings_avrdx const *) settings;

#ifndef __AVR_AVR32DB48__
# error "avlr_i2c_avrdx implementer: check portmux for this chip!"
#endif

	uint8_t sda = dxsett->pin_sda;
	uint8_t scl = dxsett->pin_scl;
	uint8_t portmux = 0, portmux_mask = 0;
	if (dxi2c->twi == &TWI0)
	{
		portmux_mask = PORTMUX_TWI0_gm;
		if (sda == PINDEF(PORTA, 2) && scl == PINDEF(PORTA, 3))
			portmux = PORTMUX_TWI0_DEFAULT_gc;
		else if (sda == PINDEF(PORTC, 2) && scl == PINDEF(PORTC, 3))
			portmux = PORTMUX_TWI0_ALT2_gc;
		else
			return -EINVAL;
	}
#ifdef TWI1
	else if (dxi2c->twi == &TWI1)
	{
		portmux_mask = PORTMUX_TWI1_gm;
		if (sda == PINDEF(PORTF, 2) && scl == PINDEF(PORTF, 3))
			portmux = PORTMUX_TWI1_DEFAULT_gc;
		else if (sda == PINDEF(PORTB, 2) && scl == PINDEF(PORTB, 3))
			portmux = PORTMUX_TWI1_ALT2_gc;
		else
			return -EINVAL;
	}
#endif
	else
		return -EINVAL;

	PORTMUX.TWIROUTEA = (PORTMUX.TWIROUTEA & ~portmux_mask) | portmux;

	dxi2c->twi->MBAUD = dxsett->mbaud;
	dxi2c->twi->MCTRLA = TWI_ENABLE_bm;
	dxi2c->twi->MSTATUS = TWI_BUSSTATE_IDLE_gc;

	return 0;
}

static int _transact(
	struct avlr_i2c * i2c,
	uint8_t addr,
	uint8_t const * data_tx,
	uint8_t data_tx_len,
	uint8_t * data_rx,
	uint8_t data_rx_len
)
{
	(void) i2c;
	(void) addr;
	(void) data_tx;
	(void) data_tx_len;
	(void) data_rx;
	(void) data_rx_len;

	// Can steal a bit from https://github.com/microchip-pic-avr-examples/avr128da48-i2c-send-receive-example/blob/master/avr128da48-i2c-send-receive-example/I2C_example/main.c

	// Send address - read, unless data_tx_len.
	// If nack, send stop, return -ENXIO (or whatever error)

	// If data_tx_len:
	//   Transmit each data byte
	//   If nack and not on the last byte, send stop, return errno

	// If data_rx_len:
	//   If we transmitted, send a repstart
	//   Receive each data byte
	//   If nack, stop receiving, send stop, return number of bytes

	// Send stop, return number of bytes

	return 0;
}
