// intellectual property is bullshit bgdc

// This module
#include "avlr_i2c_bitbang.hpp"

#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <util/delay.h>

avlr::i2c_bitbang::i2c_bitbang(avlr::gpio & sda, avlr::gpio & scl)
	: m_sda(sda), m_scl(scl), m_active(false)
{
}

void avlr::i2c_bitbang::init()
{
	m_scl.pullup(true);
	m_sda.pullup(true);

	m_scl.release();
	m_sda.release();

	m_scl.set(false);
	m_sda.set(false);
}

avlr::error avlr::i2c_bitbang::transact(
	uint8_t addr,
	uint8_t const * data_tx,
	uint8_t data_tx_len,
	uint8_t * data_rx,
	uint8_t data_rx_len
)
{
	if (data_tx_len)
	{
		_start();
		_get_put_byte(addr << 1);
		if (_get_put_bit(true))
		{
			// nack
			_stop();
			return avlr::error::ENXIO;
		}

		for (uint8_t i = 0; i < data_tx_len; i++)
		{
			_get_put_byte(data_tx[i]);
			if (_get_put_bit(true) && i < data_tx_len - 1)
			{
				// early nack
				_stop();
				return avlr::error::EIO;
			}
		}
	}

	if (data_rx_len)
	{
		_start(); // automatic repstart if needed
		_get_put_byte((addr << 1) | 1);
		if (_get_put_bit(true))
		{
			_stop();
			return avlr::error::ENXIO;
		}

		for (uint8_t i = 0; i < data_rx_len; i++)
		{
			data_rx[i] = _get_put_byte(0xFF);
			_get_put_bit(i == data_rx_len - 1);
		}
	}

	_stop();
	return avlr::error::OK;
}

void avlr::i2c_bitbang::_scl_set(bool v)
{
	m_scl.drive(!v);

	if (v)
		while (!m_scl.get());
}

void avlr::i2c_bitbang::_sda_set(bool v)
{
	m_sda.drive(!v);
}

void avlr::i2c_bitbang::_start()
{
	if (m_active)
	{
		_sda_set(true);
		_delay_us(1);
		_scl_set(true);
		_delay_us(1);
	}

	_sda_set(false);
	_delay_us(1);
	_scl_set(false);
	_delay_us(1);
	m_active = true;
}

void avlr::i2c_bitbang::_stop()
{
	_sda_set(false);
	_delay_us(1);
	_scl_set(true);
	_delay_us(1);
	_sda_set(true);
	_delay_us(1);
	m_active = false;
}

bool avlr::i2c_bitbang::_get_put_bit(bool bit)
{
	_sda_set(bit);
	_delay_us(1);
	_scl_set(true);
	_delay_us(1);
	bool bit_out = m_sda.get();
	_scl_set(false);
	_delay_us(1);
	return bit_out;
}

uint8_t avlr::i2c_bitbang::_get_put_byte(uint8_t byte)
{
	uint8_t byte_out = 0;
	for (uint8_t n = 0; n < 8; n += 1)
	{
		byte_out <<= 1;
		byte_out |= _get_put_bit(byte & 0x80) ? 1 : 0;
		byte <<= 1;
	}
	return byte_out;
}
