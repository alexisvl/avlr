// bgdc yo

#include "avlr_i2c_port.h"
#include <stdbool.h>
#include <stddef.h>
#include <inttypes.h>
#include <errno.h>

#include <avr/io.h>
#include <util/delay.h>
#include <util/atomic.h>

static int _init(
	struct avlr_i2c * i2c,
	struct avlr_i2c_settings const * settings
);

static int _transact(
	struct avlr_i2c * i2c,
	uint8_t addr,
	uint8_t const * data_tx,
	uint8_t data_tx_len,
	uint8_t * data_rx,
	uint8_t data_rx_len
);

static void _scl_set(struct avlr_i2c_port * porti2c, bool v);
static void _sda_set(struct avlr_i2c_port * porti2c, bool v);
static void _start(struct avlr_i2c_port * porti2c);
static void _stop(struct avlr_i2c_port * porti2c);
static bool _get_put_bit(struct avlr_i2c_port * porti2c, bool bit);
static uint8_t _get_put_byte(struct avlr_i2c_port * porti2c, uint8_t byte);

struct avlr_i2c * avlr_i2c_port_new(
	struct avlr_i2c_port * i2c,
	PORT_t * port,
	uint8_t bit_sda,
	uint8_t bit_scl
)
{
	i2c->pi2c.init = _init;
	i2c->pi2c.transact = _transact;
	i2c->port = port;
	i2c->bit_sda = bit_sda;
	i2c->bit_scl = bit_scl;
	return &i2c->pi2c;
}

static int _init(
	struct avlr_i2c * i2c,
	struct avlr_i2c_settings const * settings
)
{
	(void) settings;

	struct avlr_i2c_port * porti2c = (struct avlr_i2c_port *) i2c;

	uint8_t const bits = porti2c->bit_scl | porti2c->bit_sda;

	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
		porti2c->port->PINCONFIG = PORT_PULLUPEN_bm;
		porti2c->port->PINCTRLUPD = bits;
	}

	porti2c->port->DIRCLR = bits;
	porti2c->port->OUTCLR = bits;
	porti2c->active = false;
	return 0;
}

static int _transact(
	struct avlr_i2c * i2c,
	uint8_t addr,
	uint8_t const * data_tx,
	uint8_t data_tx_len,
	uint8_t * data_rx,
	uint8_t data_rx_len
)
{
	struct avlr_i2c_port * porti2c = (struct avlr_i2c_port *) i2c;

	if (data_tx_len)
	{
		_start(porti2c);
		_get_put_byte(porti2c, addr << 1);
		if (_get_put_bit(porti2c, true))
		{
			// nack
			_stop(porti2c);
			return -AVLR_I2C_ERR_NACK_ADDR;
		}

		for (uint8_t i = 0; i < data_tx_len; i++)
		{
			_get_put_byte(porti2c, data_tx[i]);
			if (_get_put_bit(porti2c, true) && i < data_tx_len - 1)
			{
				// early nack
				_stop(porti2c);
				return -AVLR_I2C_ERR_NACK_TX;
			}
		}
	}

	uint8_t n_rx = 0;

	if (data_rx_len)
	{
		_start(porti2c); // automatic repstart if needed
		_get_put_byte(porti2c, (addr << 1) | 1);
		if (_get_put_bit(porti2c, true))
			goto rxnack;

		for (n_rx = 0; n_rx < data_rx_len; n_rx++)
		{
			data_rx[n_rx] = _get_put_byte(porti2c, 0xFF);
			_get_put_bit(porti2c, false);
		}
	}

rxnack:
	_stop(porti2c);
	return n_rx;
}

static void _scl_set(struct avlr_i2c_port * porti2c, bool v)
{
	if (v)
	{
		porti2c->port->DIRCLR = porti2c->bit_scl;
		while (!(porti2c->port->IN & porti2c->bit_scl));
	}
	else
		porti2c->port->DIRSET = porti2c->bit_scl;
}

static void _sda_set(struct avlr_i2c_port * porti2c, bool v)
{
	if (v)
		porti2c->port->DIRCLR = porti2c->bit_sda;
	else
		porti2c->port->DIRSET = porti2c->bit_sda;
}

static void _start(struct avlr_i2c_port * porti2c)
{
	if (porti2c->active)
	{
		_sda_set(porti2c, true);
		_delay_us(3);
		_scl_set(porti2c, true);
		_delay_us(3);
	}

	_sda_set(porti2c, false);
	_delay_us(3);
	_scl_set(porti2c, false);
	_delay_us(3);
	porti2c->active = true;
}

static void _stop(struct avlr_i2c_port * porti2c)
{
	_sda_set(porti2c, false);
	_delay_us(3);
	_scl_set(porti2c, true);
	_delay_us(3);
	_sda_set(porti2c, true);
	_delay_us(3);
	porti2c->active = false;
}

static bool _get_put_bit(struct avlr_i2c_port * porti2c, bool bit)
{
	_sda_set(porti2c, bit);
	_delay_us(3);
	_scl_set(porti2c, true);
	_delay_us(3);
	bool bit_out = porti2c->port->IN & porti2c->bit_sda;
	_scl_set(porti2c, false);
	_delay_us(3);
	return bit_out;
}

static uint8_t _get_put_byte(struct avlr_i2c_port * porti2c, uint8_t byte)
{
	uint8_t byte_out = 0;
	for (uint8_t n = 0; n < 8; n += 1)
	{
		byte_out <<= 1;
		byte_out |= _get_put_bit(porti2c, byte & 0x80) ? 1 : 0;
		byte <<= 1;
	}
	return byte_out;
}
